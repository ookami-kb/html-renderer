import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;

public class SwingFXWebView extends JPanel {

    private Stage stage;
    private WebView browser;
    private JFXPanel jfxPanel;
    private WebEngine webEngine;

    public SwingFXWebView() {
        initComponents();
        Platform.setImplicitExit(false);
    }

    private void initComponents() {

        jfxPanel = new JFXPanel();
        createScene();

        setLayout(new BorderLayout());
        add(jfxPanel, BorderLayout.CENTER);
    }

    /**
     * createScene
     * <p>
     * Note: Key is that Scene needs to be created and run on "FX user thread"
     * NOT on the AWT-EventQueue Thread
     */
    private void createScene() {
        Platform.runLater(() -> {
            StackPane root = new StackPane();

            stage = new Stage();

            stage.setTitle("Hello Java FX");
            stage.setResizable(true);

            Scene scene = new Scene(root);
            stage.setScene(scene);

            // Set up the embedded browser:
            browser = new WebView();
            webEngine = browser.getEngine();
//            webEngine.load("http://www.google.com");

            ObservableList<Node> children = root.getChildren();
            children.add(browser);

            jfxPanel.setScene(scene);
        });
    }

    public void load(String content) {
        Platform.runLater(() -> webEngine.loadContent(content));
    }
}