import com.intellij.openapi.fileEditor.*
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import org.jdom.Element

class HtmlProvider : FileEditorProvider {
    override fun createEditor(p0: Project, p1: VirtualFile): FileEditor {
        return HtmlRenderEditor(FileDocumentManager.getInstance().getDocument(p1)!!, p0.basePath!!)
    }

    override fun writeState(p0: FileEditorState, p1: Project, p2: Element) {
    }

    override fun getPolicy(): FileEditorPolicy {
        return FileEditorPolicy.PLACE_AFTER_DEFAULT_EDITOR
    }

    override fun disposeEditor(p0: FileEditor) {
        p0.dispose()
    }

    override fun readState(p0: Element, p1: Project, p2: VirtualFile): FileEditorState {
        return FileEditorState.INSTANCE
    }

    override fun getEditorTypeId(): String {
        return "HtmlRender"
    }

    override fun accept(p0: Project, p1: VirtualFile): Boolean {
        return p1.extension == "html"
    }
}