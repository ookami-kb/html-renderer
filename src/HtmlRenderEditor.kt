import com.intellij.codeHighlighting.BackgroundEditorHighlighter
import com.intellij.ide.structureView.StructureViewBuilder
import com.intellij.openapi.editor.Document
import com.intellij.openapi.fileEditor.FileEditor
import com.intellij.openapi.fileEditor.FileEditorLocation
import com.intellij.openapi.fileEditor.FileEditorState
import com.intellij.openapi.fileEditor.FileEditorStateLevel
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.util.Key
import java.beans.PropertyChangeListener
import javax.swing.JComponent

class HtmlRenderEditor(val document: Document, val projectPath: String) : FileEditor {
    val panel = SwingFXWebView()

    override fun getName(): String {
        return "HTML"
    }

    override fun getStructureViewBuilder(): StructureViewBuilder? {
        return null
    }

    override fun setState(p0: FileEditorState) {
    }

    override fun getComponent(): JComponent {
        return panel
    }

    override fun getPreferredFocusedComponent(): JComponent? {
        return panel
    }

    override fun deselectNotify() {
    }

    override fun getBackgroundHighlighter(): BackgroundEditorHighlighter? {
        return null
    }

    override fun isValid(): Boolean {
        return true
    }

    override fun isModified(): Boolean {
        return false
    }

    override fun addPropertyChangeListener(p0: PropertyChangeListener) {
    }

    override fun getState(p0: FileEditorStateLevel): FileEditorState {
        return FileEditorState.INSTANCE
    }

    override fun selectNotify() {
        updateHtml()
    }

    override fun getCurrentLocation(): FileEditorLocation? {
        return null
    }

    override fun removePropertyChangeListener(p0: PropertyChangeListener) {
    }

    override fun <T : Any?> putUserData(p0: Key<T>, p1: T?) {
    }

    override fun <T : Any?> getUserData(p0: Key<T>): T? {
        return null
    }

    override fun dispose() {
        Disposer.dispose(this)
    }

    private fun processHtml(html: String): String {
        return html.replace(Regex("\\[\\[(.+?)\\|(.+?)\\]\\]"), "<span class='hg' data-title='$2'>$1</span>")
    }

    private fun updateHtml() {
        val text = """
        <html class="print render-plugin">
        <head>
            <link rel="stylesheet" href="file://$projectPath/mdl.min.css">
            <link rel="stylesheet" href="file://$projectPath/preview.css">
        </head>
        <body>
            <div class="container--mini mdl-grid">
                <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2dp">
                    <div class="lesson__content">
                        <div class="lesson__content">${processHtml(document.text)}</div>
                    </div>
                </div>
            </div>
        </body>
        </html>
        """

        panel.load(text)
    }
}